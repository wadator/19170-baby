(function ($, Drupal, drupalSettings) {
  
  'use strict';
  
  Drupal.behaviors.anonymous_popup_js = {
    
    attach: function (context, settings) {
        var title = drupalSettings.title;
        var description = drupalSettings.description;
        var frontpageModal = Drupal.dialog('<div>'+ description +'</div>', {
        title: title,
        dialogClass: 'anonymous-popup',
        width: 400,
        height: 400,
        autoResize: true,
        close: function (event) {
          $(event.target).remove();
        }
      });
      if (context == document) {
          frontpageModal.showModal()
      }
    }
  }
  
} (jQuery, Drupal, drupalSettings));
