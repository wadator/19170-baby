<?php

/**
 * @file
 * Contains popup_entity.page.inc.
 *
 * Page callback for Popup entity entities.
 */

use Drupal\Core\Render\Element;

/**
 * Prepares variables for Popup entity templates.
 *
 * Default template: popup_entity.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - elements: An associative array containing the user information and any
 *   - attributes: HTML attributes for the containing element.
 */
function template_preprocess_popup_entity(array &$variables) {
  // Fetch PopupEntity Entity Object.
  $popup_entity = $variables['elements']['#popup_entity'];

  // Helpful $content variable for templates.
  foreach (Element::children($variables['elements']) as $key) {
    $variables['content'][$key] = $variables['elements'][$key];
  }
}
