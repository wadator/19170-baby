<?php

namespace Drupal\anonymous_popup\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Popup entity entities.
 *
 * @ingroup anonymous_popup
 */
class PopupEntityDeleteForm extends ContentEntityDeleteForm {


}
