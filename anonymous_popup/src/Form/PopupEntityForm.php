<?php

namespace Drupal\anonymous_popup\Form;

use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Form controller for Popup entity edit forms.
 *
 * @ingroup anonymous_popup
 */
class PopupEntityForm extends ContentEntityForm {

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    /* @var $entity \Drupal\anonymous_popup\Entity\PopupEntity */
    $form = parent::buildForm($form, $form_state);

    $entity = $this->entity;

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $entity = $this->entity;

    $status = parent::save($form, $form_state);

    switch ($status) {
      case SAVED_NEW:
        drupal_set_message($this->t('Created the %label Popup entity.', [
          '%label' => $entity->label(),
        ]));
        break;

      default:
        drupal_set_message($this->t('Saved the %label Popup entity.', [
          '%label' => $entity->label(),
        ]));
    }
    $form_state->setRedirect('entity.popup_entity.canonical', ['popup_entity' => $entity->id()]);
  }

}
