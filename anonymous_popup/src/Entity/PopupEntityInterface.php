<?php

namespace Drupal\anonymous_popup\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface for defining Popup entity entities.
 *
 * @ingroup anonymous_popup
 */
interface PopupEntityInterface extends ContentEntityInterface, EntityChangedInterface, EntityOwnerInterface {

  // Add get/set methods for your configuration properties here.

  /**
   * Gets the Popup entity name.
   *
   * @return string
   *   Name of the Popup entity.
   */
  public function getName();

  /**
   * Sets the Popup entity name.
   *
   * @param string $name
   *   The Popup entity name.
   *
   * @return \Drupal\anonymous_popup\Entity\PopupEntityInterface
   *   The called Popup entity entity.
   */
  public function setName($name);

  /**
   * Gets the Popup entity creation timestamp.
   *
   * @return int
   *   Creation timestamp of the Popup entity.
   */
  public function getCreatedTime();

  /**
   * Sets the Popup entity creation timestamp.
   *
   * @param int $timestamp
   *   The Popup entity creation timestamp.
   *
   * @return \Drupal\anonymous_popup\Entity\PopupEntityInterface
   *   The called Popup entity entity.
   */
  public function setCreatedTime($timestamp);

  /**
   * Returns the Popup entity published status indicator.
   *
   * Unpublished Popup entity are only visible to restricted users.
   *
   * @return bool
   *   TRUE if the Popup entity is published.
   */
  public function isPublished();

  /**
   * Sets the published status of a Popup entity.
   *
   * @param bool $published
   *   TRUE to set this Popup entity to published, FALSE to set it to unpublished.
   *
   * @return \Drupal\anonymous_popup\Entity\PopupEntityInterface
   *   The called Popup entity entity.
   */
  public function setPublished($published);

}
