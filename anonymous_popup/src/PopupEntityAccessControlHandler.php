<?php

namespace Drupal\anonymous_popup;

use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Access\AccessResult;

/**
 * Access controller for the Popup entity entity.
 *
 * @see \Drupal\anonymous_popup\Entity\PopupEntity.
 */
class PopupEntityAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {
    /** @var \Drupal\anonymous_popup\Entity\PopupEntityInterface $entity */
    switch ($operation) {
      case 'view':
        if (!$entity->isPublished()) {
          return AccessResult::allowedIfHasPermission($account, 'view unpublished popup entity entities');
        }
        return AccessResult::allowedIfHasPermission($account, 'view published popup entity entities');

      case 'update':
        return AccessResult::allowedIfHasPermission($account, 'edit popup entity entities');

      case 'delete':
        return AccessResult::allowedIfHasPermission($account, 'delete popup entity entities');
    }

    // Unknown operation, no opinion.
    return AccessResult::neutral();
  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL) {
    return AccessResult::allowedIfHasPermission($account, 'add popup entity entities');
  }

}
