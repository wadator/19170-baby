<?php

namespace Drupal\anonymous_popup;

use Drupal\content_translation\ContentTranslationHandler;

/**
 * Defines the translation handler for popup_entity.
 */
class PopupEntityTranslationHandler extends ContentTranslationHandler {

  // Override here the needed methods from ContentTranslationHandler.

}
